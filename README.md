# La Nassa Website

A new re-designed website for la nassa.

We started from two Bootstrap templates [1, 2] and then diverted to a fully customized version.

The website is static and designed using basic HTML and CSS and vanilla JS.
No server-side, no cookies, no saving, nothing.

The water effect on the home page is done using the ripples effect from the [jQuery Ripples Plugin](https://github.com/sirxemic/jquery.ripples) [3]

A porting in Angular is welcome and if we manage, we will do it at some point.


#### original sources:
[1] - [Grayscale](https://startbootstrap.com/theme/grayscale) for the general template

[2] - [Agency](https://startbootstrap.com/theme/agency) for some features (timeline and pics)

[3] - jQuery Ripples Plugin - [Github Repo](https://github.com/sirxemic/jquery.ripples) - [demo](jQuery Ripples Plugin)
