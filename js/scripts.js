/*!
    * Start Bootstrap - Grayscale v6.0.3 (https://startbootstrap.com/theme/grayscale)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-grayscale/blob/master/LICENSE)
    */
    (function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate(
                    {
                        scrollTop: target.offset().top - 70,
                    },
                    1000,
                    "easeInOutExpo"
                );
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
        
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $("body").scrollspy({
        target: "#mainNav",
        offset: 100,
    });

    
    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            //$("#mainNav").addClass("navbar-shrink");
            $("#mainNav").addClass("always-on-top");
            $('#top-left-logo').height('50px');
            $('#top-left-logo').attr("src",'/assets/img/logo/Logo_NoLettering.svg');
            if (actual_page == 'index.html' || actual_page == "" || actual_page == "#page-top") {
              blackify();
            }
        } else {
            //$("#mainNav").removeClass("navbar-shrink");
            $("#mainNav").removeClass("always-on-top");
            $('#top-left-logo').height('80px');
            if (actual_page == 'index.html' || actual_page == "" || actual_page == "#page-top") {
              whitify();
            }
            else {
              $('#top-left-logo').attr("src",'/assets/img/logo/Logo_Orizzontale.svg');
            }
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);
})(jQuery); // End of use strict

var actual_page = window.location.href.split("/").pop();
var navlinks = $('.nav-link');
var menu_toggle = 'bars';
var j = 0;

function whitify() {
  $('#top-left-logo').attr("src",'/assets/img/logo/Logo_Orizzontale_Bianco.svg');
  for (j = 0; j < navlinks.length; j++) {
    navlinks[j].style.color = 'white';
  }
  $('#btn-menu')[0].style.color = 'white';
}

function blackify() {
  for (j = 0; j < navlinks.length; j++) {
    navlinks[j].style.color = 'black';
  }
  $('#btn-menu')[0].style.color = 'black';
}
if (actual_page == 'index.html' || actual_page == "" || actual_page == "#page-top") {
  whitify(); 
}

$(document).on("click", "#btn-menu", function() {
  if (menu_toggle == 'bars') {
    $('#btn-menu').html('<i class="nassa-text-large fas fa-times"></i>');
    menu_toggle = 'times';
    console.log('changed to times');
    $('#mainNav').removeClass('transparent');
    if (actual_page == 'index.html' || actual_page == "" || actual_page == "#page-top") {
      blackify(); 
    }
  }
  else if (menu_toggle == 'times') {
    $('#btn-menu').html('<i class="nassa-text-large fas fa-bars"></i>');
    menu_toggle = 'bars';
    console.log('changed to bars');
    $('#mainNav').addClass('transparent');
    if (actual_page == 'index.html' || actual_page == "" || actual_page == "#page-top") {
      whitify(); 
    }
  }
});


